/**
 * <h1>Server</h1>
 * <p>The Server class is the top class for the EZShare.client.server side.</p>
 * <p>In the EZShare.client.server class, we use the ServerSocketFactory to create a socket factory to read/save all the command user gave from EZShare.client,</p>
 * <p>and use the sendClientMessage class to connect to the EZShare.client and send the response.</p>
 * <p>Then, we use ServerRunnable class to spawn a new thread to take care of the new request.</p>
 * <p>And the servers will exchange the Serverlist to others automatically by using the exchangeServer and exchangeBetweenServer.</p>
 * 
 * 	 
 * @author Group 88888
 * @version 1.0
 * 
 */
package EZShare;

import EZShare.common.ClientCommand;
import EZShare.common.ClientConnection;
import EZShare.common.ServerAddress;
import EZShare.common.ServerCommand;
import EZShare.logger.LogType;
import EZShare.logger.Logger;
import EZShare.server.*;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.ParseException;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;

public class Server {

    // Identifies the user number connected
    private static int counter = 0;
    private static ServerAddressContainer unsecuredServers;
    private static ServerAddressContainer SSLServers;
    private static ConcurrentHashMap<InetAddress, Long> serverTimes;

    public static void main(String[] args) {

        //Following set of keylines coppied from tutes
        //Specify the keystore details (this can be specified as VM arguments as well)
        //the keystore file contains an application's own certificate and private key


		System.setProperty("javax.net.ssl.keyStore", "serverKeys/server88888.jks");
        //Password to access the private key from the keystore file
        System.setProperty("javax.net.ssl.keyStorePassword", "team88888");


		System.setProperty("javax.net.ssl.trustStore", "server88888/server88888.jks");

		System.setProperty("javax.net.sll.trustStorePassword", "team88888");



        Config configuration;
		try {
			configuration = new Config(args);//read from the input
            Logger.initialize("Server", configuration.isDebug());
            Logger.log(LogType.INFO, "Parsed Arguments");
		} catch (ParseException e) {
		    Logger.initialize("Server", false);
			Logger.log(LogType.INFO,"Error parsing arguments");
            Logger.flush();
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("EZServer Server", Config.getOptions());
			return;
		}
        Logger.log(LogType.INFO, "SSL debugging currently turned off");

		// Enable debugging to view the handshake and communication which happens between the SSLClient and the SSLServer
		//System.setProperty("javax.net.debug","all");

		serverTimes = new ConcurrentHashMap<>();
        Logger.log(LogType.INFO, "Connection interval limit is " + configuration.getConnectionIntervalLimit() * 1000 + "ms");

		ConcurrentHashMap<Integer,SubscriptionConnection> subscriptionConnections = new ConcurrentHashMap<>();

		ResourceContainer resources = new ResourceContainer(subscriptionConnections);
        unsecuredServers = new ServerAddressContainer();
        SSLServers = new ServerAddressContainer();

        InetAddress newAddress;

        Logger.log(LogType.INFO, String.format("Receiving messages at %s:%d", configuration.getAdvertisedHostname(), configuration.getPort()));

        // Adding this server to severList so when this server exchanges with another, that server will have a record of this server
        unsecuredServers.addServer(configuration.getServerAddress(false));
        SSLServers.addServer(configuration.getServerAddress(true));
        //create factory by using the port, and create actually socket by using the factory

        // Exchange between non-ssl servers
        new Thread(new ServerFunction(unsecuredServers, configuration, false)).start(); //TODO maybe can incorporate in while loop but would need to get rid of while loop inside function

        // Exchange between ssl servers
        new Thread(new ServerFunction(SSLServers, configuration, true)).start(); //TODO maybe can incorporate in while loop but would need to get rid of while loop inside function

        // non-ssl thread
        new Thread(new ServerDispatcher(configuration, resources, unsecuredServers, serverTimes, subscriptionConnections, false)).start();

        // ssl thread
        new Thread(new ServerDispatcher(configuration, resources, SSLServers, serverTimes, subscriptionConnections, true)).start();
        Logger.flush();//Not sure what this does, something something Leo + multithreading
    }

    
    public static void exchangeServer(ServerAddress targetServer, ServerCommand serverCommand, ServerAddressContainer servers, boolean SSLvalue) {
    	ClientCommand clientCommand = new ClientCommand(serverCommand.getCommandType(), targetServer, null, servers.getServers(), false, SSLvalue);
    	ClientConnection clientConnection = new ClientConnection(clientCommand, true);
    	if (!clientConnection.responseSuccessful()) {
    		// Connection failed
    		//TODO what if a later point had a failure like passing an invalid resource?
    		servers.removeServerAddress(targetServer);
    	}
    }
    
    public static void sendClientMessage(Socket clientSocket, String message) throws IOException {
		/* Sends a message to a EZShare.client */
		Logger.log(LogType.SENT, String.format("%s",message));
    	DataOutputStream output = new DataOutputStream(clientSocket.getOutputStream());
        output.writeUTF(message);
    }    

}
