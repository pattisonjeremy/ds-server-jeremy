/** 
<h2>ServerRunnable</h2>
 * <p>The ServerList class stores the connect list for each EZShare.client.server in the ServerListContainer.</p>
 * <p>In the Server class, the exchangeServer will use the ServerList to send exchange command to servers in this list.</p>
 * <p>And use the function addServers and removeServerAddress to refresh the list</p>
 
 *@author Group 88888
 *@version 1.0
 *@throws IOException
 */
package EZShare.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import EZShare.Server;
import EZShare.common.*;
import EZShare.logger.LogType;
import EZShare.logger.Logger;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;


public class ServerRunnable implements Runnable {

    private Socket socket;
    private ResourceContainer resources;
    private ServerAddressContainer servers;
    private boolean isSSL;
    Config config;
    private ConcurrentHashMap<Integer,SubscriptionConnection> subscriptions;

    public ServerRunnable(Socket newSocket, Config newConfig, ResourceContainer newResources, ServerAddressContainer newServers, boolean newIsSSL, ConcurrentHashMap<Integer,SubscriptionConnection> newSubscriptions) {
        socket = newSocket;
        config = newConfig;
        resources = newResources;
        servers = newServers;
        isSSL = newIsSSL;
        subscriptions = newSubscriptions;
    }

    public void run() {
        // Use try-with-resources with the socket to ensure that it will
        // be closed regardless of any Exceptions encountered. As such, it is only
        // needed once, in this function.
        try (Socket clientSocket = socket) {
            DataInputStream input = new DataInputStream(clientSocket.getInputStream());
    		Logger.log(LogType.INFO, String.format((isSSL ? "Secured" : "Unsecured") + " connection made with %s:%s", clientSocket.getInetAddress(), clientSocket.getPort()));
            String message = input.readUTF();
    		Logger.log(LogType.RECEIVED, String.format("%s",message));
            ServerCommand cmd = new ServerCommand(message, config);

            Logger.log(LogType.INFO, cmd.getCommandType() + " command");
            
            //Deal with the different types of command
            switch (cmd.getCommandType()) {

                case PUBLISH:
                    respondPublish(clientSocket, cmd);
                    break;

                case REMOVE:
                    respondRemove(clientSocket, cmd);
                    break;

                case SHARE:
                    respondShare(clientSocket, cmd);
                    break;

                case QUERY:
                    respondQuery(clientSocket, cmd);
                    break;

        		case FETCH:
                    respondFetch(clientSocket, cmd);
        			break;

                case EXCHANGE:
                    respondExchange(clientSocket, cmd);
                	break;

                // TODO: UNSUBSCRIBE should never appear here since a SUBSCRIBE has to precede it
                case UNSUBSCRIBE:
                case SUBSCRIBE:
                	respondPersistent(clientSocket, cmd);
                	break;
                	
                case INVALID:
                    Server.sendClientMessage(clientSocket, Responses.invalidCommand().toString());
                    break;

                default:
                case MISSING:
                    Server.sendClientMessage(clientSocket, Responses.missingCommand().toString());
                    break;
            }

        } catch (IOException e) {
            Logger.log(LogType.ERROR, "IOException in response thread: " + e.getMessage());
        } catch (ParseException e) {
			// TODO Auto-generated catch block
            Logger.log(LogType.ERROR, "Could not incoming message: " + e.getMessage());
		} finally {
            // Close the socket if it hasn't been passed away
            //if (!socketPassedElsewhere) {
            //    int closeAttemptsRemaining = 10;
            //    while (closeAttemptsRemaining > 0) {
            //        if (clientSocket != null) {
            //            try {
            //                clientSocket.close();
            //                closeAttemptsRemaining = 0;
            //            } catch (IOException e) {
            //                Logger.log(
            //                        LogType.ERROR,
            //                        String.format(
            //                                "Could not close connection to %s:%s",
            //                                clientSocket.getInetAddress(),
            //                                clientSocket.getPort()
            //                        )
            //                );
            //            }
            //        }
            //        closeAttemptsRemaining -= 1;
            //        try {
            //            Thread.sleep(1000);
            //        } catch (InterruptedException e) {
            //            // Do nothing
            //        }
            //    }
            //}
        }
        
    }


    private void respondUnsubscribe(Socket clientSocket, ServerCommand cmd) throws IOException {
    	Resource queryResourceTemplate = cmd.getResource();
        
            ArrayList<Resource> matches;
            matches = resources.findResourceByTemplate(queryResourceTemplate);
            queryServers(matches, cmd);
     
            Server.sendClientMessage(clientSocket, Responses.resultSize(matches.size()).toString());
        
    }

	private void respondPublish(Socket clientSocket, ServerCommand cmd) throws IOException {
        JSONObject response;
        Resource publishResource = cmd.getResource();
        if (publishResource == null) {
            response = Responses.missingResource();
        } else if (publishResource.isValidPublish()) {
            boolean flag = resources.addResource(publishResource);
            if (flag) {
                response = Responses.success();
                resources.getAllAsString();
            } else {
                response = Responses.invalidResource();
            }
        } else {
            response = Responses.invalidResource();
        }

        Server.sendClientMessage(clientSocket, response.toString());//not sure which one should be a good response

    }

    private void respondShare(Socket clientSocket, ServerCommand cmd) throws IOException {
        JSONObject response = new JSONObject();
        Resource shareResource = cmd.getResource();
        if (!cmd.hasSecret() || shareResource == null) {
            response = Responses.missingResourceOrSecret();
        } else if (!(cmd.getSecret().equals(config.getSecret()))) {
            response = Responses.incorrectSecret();
        } else if (!shareResource.isValidShare()) {
            response = Responses.invalidResource();
        } else {
            boolean flag = resources.addResource(shareResource);
            if (flag) {
                response = Responses.success();
                resources.getAllAsString();
            } else {
                response = Responses.cannotShareResource();
            }
        }
        Server.sendClientMessage(clientSocket, response.toString());
    }

    public void respondFetch(Socket clientSocket, ServerCommand cmd) throws IOException {
        DataOutputStream output = new DataOutputStream(clientSocket.getOutputStream());
        Resource resourceTemplate = cmd.getResource();
        if (resourceTemplate == null) {
            Server.sendClientMessage(clientSocket, Responses.missingResourceTemplate().toString());
        } else if (!resourceTemplate.isValidFetch()) {
            Logger.log(LogType.ERROR, "Invalid resourceTemplate for FETCH");
            Server.sendClientMessage(clientSocket, Responses.invalidResourceTemplate().toString());
        } else {
            Resource fetchResource = resources.findResource(resourceTemplate);
            if (fetchResource == null) {
                Logger.log(LogType.ERROR, "Could not find resource for FETCH");
                Server.sendClientMessage(clientSocket, Responses.invalidResourceTemplate().toString());
            } else {
                JSONObject jsonResource = new JSONObject();
                fetchResource = fetchResource.cloneWithFileSizeFromUri();
                // Theoretically a better solution would include a loop
                if (fetchResource.getFileSizeAtUri() > 0) {
                    // First response should be {"response": "success"}
                    Server.sendClientMessage(clientSocket, Responses.success().toString());

                    // Next, send the Resource as JSON
                    jsonResource = fetchResource.toJson();
                    int resourceSize = fetchResource.getResourceSize();
                    jsonResource.put("resourceSize", resourceSize);
                    try {
                        Server.sendClientMessage(clientSocket, jsonResource.toJSONString());
                        // Finally, send the file itself
                        FileSender fileTransporter = new FileSender(URICreator.createUri(fetchResource.getUri()));
                        fileTransporter.sendFile(output);
                    } catch (URISyntaxException e) {
                        // If it gets to this point something has gone catastrophically wrong
                        // because we've already checked the Resource's URI for validity
                        Logger.log(LogType.ERROR, "Mysterious URI error");
                    }

                    // Finally, send {"resultSize": 1}
                    Server.sendClientMessage(clientSocket, Responses.resultSize(1).toString());

                }
            }
        }
    }

    public void respondQuery(Socket clientSocket, ServerCommand cmd) throws IOException {
        // TODO: Ensure that the owner of the Response is never revealed, WITHOUT MUTATING THE RESOURCE OBJECT!!!
        Resource queryResourceTemplate = cmd.getResource();
        if (queryResourceTemplate == null) {
            Server.sendClientMessage(clientSocket, Responses.missingResourceTemplate().toString());
        } else if (!queryResourceTemplate.isValidTemplate()) {
            Server.sendClientMessage(clientSocket, Responses.invalidResourceTemplate().toString());
        } else {
            ArrayList<Resource> matches;
            matches = resources.findResourceByTemplate(queryResourceTemplate);
            Logger.log(LogType.DEBUG, "Begin query servers");
            queryServers(matches, cmd);
            Logger.log(LogType.DEBUG, "Servers queries");
            Server.sendClientMessage(clientSocket, Responses.success().toString());
            for (Resource match : matches) {
                Server.sendClientMessage(clientSocket, match.cloneWithAnonymousOwner().toJson().toString());
            }
            Server.sendClientMessage(clientSocket, Responses.resultSize(matches.size()).toString());
        }
    }

    private void respondPersistent(Socket clientSocket, ServerCommand cmd) throws IOException {

            SubscriptionConnection subscriptionsOnMySocket = new SubscriptionConnection(clientSocket, config, cmd, servers);
            int subscriptionConnectionId = resources.getNewConnectionId();

            // Add this subscription connection to resources. Hereafter, whenever the contents of the ResourceContainer
            // are changed, it will add the added Resource to the queue of our SubscriptionConnection

            resources.addSubscriptionConnection(subscriptionConnectionId, subscriptionsOnMySocket);

            // Run the connection. It will loop between reading and writing until it's been fully unsubscribed from,
            // and handle all responses

            subscriptionsOnMySocket.run();

            // Deregister the subscription connection from resources

            resources.removeSubscriptionConnection(subscriptionConnectionId);


            //Server.sendClientMessage(clientSocket, Responses.successWithId(s.getUniqueID()).toString());

//            for (SubscriptionConnection match : matcheSub) {
            //Server.sendClientMessage(clientSocket, s.getResourceTemplate().cloneWithAnonymousOwner().toJson().toString());
//            }
    }

    public void respondRemove(Socket clientSocket, ServerCommand cmd) throws IOException {
        Resource removeResource = cmd.getResource();
        JSONObject response;

        if (removeResource == null) {
            response = Responses.missingResource();
        } else if (!removeResource.isValidRemove()) {
            response = Responses.invalidResource();
        } else if (resources.removeResource(removeResource)) {
            response = Responses.success();
        } else {
            response = Responses.cannotRemoveResource();
        }

        Server.sendClientMessage(clientSocket, response.toString());
        resources.getAllAsString(); // May print outdated info
    }

    public void respondExchange(Socket clientSocket, ServerCommand cmd) throws IOException {
        //Validate message for errors
        ArrayList<ServerAddress> inputServers = cmd.getServers();
        if (inputServers == null) {
            Server.sendClientMessage(clientSocket, Responses.invalidOrMissingServerList().toString());
        } else if (cmd.hasInvalidServerInList()) {
            Server.sendClientMessage(clientSocket, Responses.missingResourceTemplate().toString());
        } else {
            servers.addServers(cmd.getServers());
            Server.sendClientMessage(clientSocket, Responses.success().toString());
        }
    }

    public void queryServers(ArrayList<Resource > matches, ServerCommand serverCommand){
        Logger.log(LogType.DEBUG, "Getting server list");
    	List<ServerAddress> serverList = servers.getServers();
    	Logger.log(LogType.DEBUG, "Got server list");
    	if(serverList.size()<=0 || serverCommand.getRelay() == false) {
    		return;
    	}
    	ClientConnection clientConnection;
    	ClientCommand clientCommand;
    	for (ServerAddress server: serverList) {
    	    if (config.getServerAddress(isSSL).equals(server)) {
    	        continue;
            }
    		clientCommand = new ClientCommand(serverCommand.getCommandType(), server, serverCommand.getResource(), servers.getServers(), false, isSSL);
    		Logger.log(LogType.DEBUG, "Making connection to " + server.toString());
    		clientConnection = new ClientConnection(clientCommand, true);
            Logger.log(LogType.DEBUG, "Connection completed");
    		if ((clientConnection.responseSuccessful())) {
    			if (clientConnection.getResourceList() != null && clientConnection.getResourceList().size() > 0) { //Double check case if ArrayList exists but is empty
    				for (Resource tempResource : clientConnection.getResourceList()) {
    					matches.add(tempResource);
    				}
    			}
    			
    		}
    	}
    	
    }



}