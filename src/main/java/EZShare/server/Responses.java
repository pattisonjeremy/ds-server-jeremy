/**
 * <h2>Responses</h2>
 * <p>The Response class is a collection of different kind of response </p>
 * <p>Other classes can use this class to create response.</p>
 * 
 * @author Group 88888
 * @version 1.0
 * 
 */
package EZShare.server;

import org.json.simple.JSONObject;

// Util class for constructing responses according the the spec
public class Responses {
    public static JSONObject success() {
        JSONObject response = new JSONObject();
        response.put("response", "success");
        return response;
    }

    public static JSONObject successWithId(String id) {
        JSONObject response = success();
        response.put("id", id);
        return response;
    }

    private static JSONObject error() {
        JSONObject response = new JSONObject();
        response.put("response", "error");
        return response;
    }

    public static JSONObject missingResource() {
        JSONObject response = error();
        response.put("errorMessage", "missing resource");
        return response;
    }
    
    public static JSONObject missingID() {
        JSONObject response = error();
        response.put("errorMessage", "missing ID");
        return response;
    }
    

    public static JSONObject missingResourceTemplate() {
        JSONObject response = error();
        response.put("errorMessage", "missing resourceTemplate");
        return response;
    }

    public static JSONObject invalidResourceTemplate() {
        JSONObject response = error();
        response.put("errorMessage", "invalid resourceTemplate");
        return response;
    }
    
    public static JSONObject invalidID() {
        JSONObject response = error();
        response.put("errorMessage", "invalid id");
        return response;
    }

    public static JSONObject missingResourceOrSecret() {
        JSONObject response = error();
        response.put("errorMessage", "missing resource and/or secret");
        return response;
    }

    public static JSONObject invalidResource() {
        JSONObject response = error();
        response.put("errorMessage", "invalid resource");
        return response;
    }

    public static JSONObject incorrectSecret() {
        JSONObject response = error();
        response.put("errorMessage", "incorrect secret");
        return response;
    }

    public static JSONObject cannotShareResource() {
        JSONObject response = error();
        response.put("errorMessage", "cannot share resource");
        return response;
    }

    public static JSONObject cannotRemoveResource() {
        JSONObject response = error();
        response.put("errorMessage", "cannot remove resource");
        return response;
    }

    public static JSONObject resultSize(int i) {
        JSONObject response = new JSONObject();
        response.put("resultSize", i);
        return response;
    }

    public static JSONObject invalidOrMissingServerList() {
        JSONObject response = error();
        response.put("errorMessage", "missing or invalid server list");
        return response;
    }

    public static JSONObject missingCommand() {
        JSONObject response = error();
        response.put("errorMessage", "missing or incorrect type for command");
        return response;
    }

    public static JSONObject invalidCommand() {
        JSONObject response = error();
        response.put("errorMessage", "invalid command");
        return response;
    }

    public static JSONObject unknownError() {
        JSONObject response = error();
        response.put("errorMessage", "unknown error");
        return response;
    }
}
