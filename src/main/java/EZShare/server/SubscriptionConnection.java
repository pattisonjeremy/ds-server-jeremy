package EZShare.server;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import EZShare.Server;
import EZShare.common.CommandType;
import EZShare.common.ServerCommand;
import EZShare.logger.LogType;
import EZShare.logger.Logger;
import EZShare.common.ServerAddress;
import org.json.simple.JSONObject;

import EZShare.common.Resource;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

// Class representing one subscribed client and all of their sub-subscriptions
public class SubscriptionConnection {
	private boolean subscribe=true;
	private HashMap<String, Resource> subscriptions = new HashMap<>();
	private Socket socket;
	private int numItemsSentOut = 0;
	private Config config;
	private ConcurrentLinkedQueue<Resource> incomingResources = new ConcurrentLinkedQueue<>();
	private ServerAddressContainer servers;


	public SubscriptionConnection(Socket newSocket, Config newConfig, ServerCommand command, ServerAddressContainer newServers) throws IOException {
		socket = newSocket;
		config = newConfig;
		servers = newServers;
		respond(command);
	}

	public void respond(ServerCommand cmd) throws IOException {
		CommandType commandType = cmd.getCommandType();
		if (commandType == CommandType.SUBSCRIBE) {
			respondSubscribe(cmd);
		} else if (commandType == CommandType.UNSUBSCRIBE) {
		    respondUnsubscribe(cmd);
		}
	}

	public void respondSubscribe(ServerCommand cmd) throws IOException {
		Resource resourceTemplate = cmd.getResource();
		String id = cmd.getId();
		if (resourceTemplate == null) {
			Server.sendClientMessage(socket, Responses.missingResourceTemplate().toString());
		} else if (!resourceTemplate.isValidTemplate()) {
			Server.sendClientMessage(socket, Responses.invalidResourceTemplate().toString());
		} else if (cmd.idHasTypeError()) {
			Server.sendClientMessage(socket, Responses.invalidID().toString());
		} else if (id == null) {
			Server.sendClientMessage(socket, Responses.missingID().toString());
		} else {
			if (addSubscription(id, resourceTemplate, cmd.getRelay(),null)) {
				Server.sendClientMessage(socket, Responses.successWithId(id).toString());
			} else {
				Server.sendClientMessage(socket, Responses.unknownError().toString());
			}
		}
	}

	public void respondUnsubscribe(ServerCommand cmd) throws IOException {
		String id = cmd.getId();
		if (id == null) {
			Server.sendClientMessage(socket, Responses.missingID().toString());
		} else if (cmd.idHasTypeError()){
			Server.sendClientMessage(socket, Responses.invalidID().toString());
		} else {
			if (removeSubscription(id)) {
			    if (subscriptions.isEmpty()) {
					Server.sendClientMessage(socket, Responses.resultSize(numItemsSentOut).toString());
					// Loop will terminate if subscriptions is empty after this point
				}
			}
		}

	}


    public boolean addSubscription(String id, Resource resourceTemplate, boolean relay, ConcurrentHashMap<Integer,SubscriptionConnection> subscriptionContainer) {
        if (subscriptions.putIfAbsent(id, resourceTemplate) == null) {
			//if (relay) {
			//	for (ServerAddress server : servers.getServers()) {
			//		server.addSubscription(resourceTemplate, subscriptionContainer);
			//
			//
			//	}
			//}
            return true;
        } else {
            return false;
        }
    }

    public boolean removeSubscription(String id) {
		if (subscriptions.remove(id) != null) {
			return true;
		} else {
			return false;
		}
	}

	public void addToIncomingResources(Resource resource) {
		incomingResources.add(resource);
	}

	public ArrayList<String> getMatchingSubscriptionIds(Resource resource) {
		ArrayList<String> matchingIds = new ArrayList<>();
		for (Map.Entry<String,Resource> entry : subscriptions.entrySet()) {
		    if (resource.matchesResourceTemplate(entry.getValue())) {
		    	matchingIds.add(entry.getKey());
			}
        }
        return matchingIds;
	}

	public void run() {
	    try {
			DataInputStream inputStream = new DataInputStream(socket.getInputStream());
			JSONParser parser = new JSONParser();
			socket.setSoTimeout(200);
			while (true) {
				// Read
				try {
					String message = inputStream.readUTF();
					try {
						ServerCommand command = new ServerCommand((JSONObject) parser.parse(message), config);
						respond(command);
						if (subscriptions.isEmpty()) {
							break;
						}
					} catch (ParseException e) {
						// TODO: Handle malformed JSON
					}
				} catch (SocketTimeoutException e) {

				}
				// Write
                // Whenever a Resource is added to the ResourceContainer, it adds them to the incomingResources queue
				// We check here whether any new Resources have been added to the queue since the last iteration,
				// then we check whether any of our subscriptions match this new Resource and send them to the
				// client
                while (!incomingResources.isEmpty()) {
					Resource incomingResource = incomingResources.poll();
					ArrayList<String> matchingIds = getMatchingSubscriptionIds(incomingResource);
					if (!matchingIds.isEmpty()) {
						DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());
						outputStream.writeUTF(incomingResource.toJson().toJSONString());
						Logger.log(LogType.SENT, incomingResource.toJson().toJSONString());
						numItemsSentOut += 1;
					}
				}
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					// Do nothing
				}

			}
		} catch (IOException e) {
		}
	}

	//public boolean isRelay() {
	//	return relay;
	//}

	//private void setRelay(boolean relay) {
	//	this.relay = relay;
	//}
}

