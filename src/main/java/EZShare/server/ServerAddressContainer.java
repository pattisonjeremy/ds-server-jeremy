/**
 * <h2>ServerList</h2>
 * <p>The ServerList class stores the connect list for each EZShare.client.server in the ServerListContainer.</p>
 * <p>In the Server class, the exchangeServer will use the ServerList to send exchange command to servers in this list.</p>
 * <p>And use the function addServers and removeServerAddress to refresh the list</p>
 * 
 * @author Group 88888
 * @version 1.0
 * 
 */
package EZShare.server;

import EZShare.common.ServerAddress;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import EZShare.logger.LogType;
import EZShare.logger.Logger;

public class ServerAddressContainer {

    private List<ServerAddress> servers = Collections.synchronizedList(new ArrayList<>());

    public ServerAddressContainer() {}

    public boolean addServer(ServerAddress server) {
        if (server.isValid()) {
            ServerAddress conflictingServerList = findServerAddress(server);
            if (conflictingServerList == null) {
                synchronized (servers) {
                    Logger.log(LogType.INFO, String.format("ServerContainer added: %s", server.toString()));
                    servers.add(server);
                    return true;
                }
            }
        }
        return false;
    }
    
    public ServerAddress findServerAddress(ServerAddress inputServer) {
    	ServerAddress outputServer = null;
        synchronized (servers) {
            for (ServerAddress server : servers) {
                if (server.serverMatches(inputServer)) {
                    outputServer = server;
                    break;
                }
            }
            return outputServer;
        }
    }

    // TODO: Make this return a clone
    public List<ServerAddress> getServers(){
    	// Returns all severs 
    	return servers;
    }

    public boolean removeServerAddress(ServerAddress inputServerList) {
        int foundIndex = -1;
        synchronized (servers) {
            for (int i = 0; i < servers.size(); i++) {
                if (servers.get(i).serverMatches(inputServerList)) {
                    foundIndex = i;
                    break;
                }
            }
            if (foundIndex > -1) {
                servers.remove(foundIndex);
            }
        }
        return foundIndex > -1;
    }
    
    public void addServers(ArrayList<ServerAddress> servers) {
    	//Adds a new EZShare.client.server to ServerAddressContainer
    	for(ServerAddress newServer : servers) {
    		addServer(newServer);
    	}
    }

    public ServerAddress getRandom() {
        if (servers.size() > 0) {
            Random rn = new Random();
            int serverPos = rn.nextInt(servers.size());
            return servers.get(serverPos);
        }
        else {
            return null;
        }
    }

}
