/**
 * <h2>Resource Container</h2>
 * <p>The Resource Container class let the EZShare.client.server can store the resources.</p>
 * <p>So the EZShare.client.server an add, search, remove, print the resources by using the Resource Container class</p>
 * 
 * @author Group 88888
 * @version 1.0
 * 
 */
package EZShare.server;

import EZShare.common.Resource;
import EZShare.logger.LogType;
import EZShare.logger.Logger;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ResourceContainer {

    // This is thread safe as long as you treat the Resources inside as immutable, WHICH YOU SHOULD
    private ConcurrentHashMap<String,Resource> resources = new ConcurrentHashMap<>();
    private ConcurrentHashMap<Integer,SubscriptionConnection> subscriptionConnections;
    private int highestSocketId = 0;

    public ResourceContainer(ConcurrentHashMap<Integer,SubscriptionConnection> subscriptionConnections) {
        this.subscriptionConnections=subscriptionConnections;
    }

    public boolean addResource(Resource resource) {
        resources.put(resource.getPrimaryKey(), resource);
        // Add the resource to the input queue of all the SubscriptionConnections
        for (SubscriptionConnection subscriptionConnection : subscriptionConnections.values()) {
            subscriptionConnection.addToIncomingResources(resource);
        }
        return true;
    }

    public synchronized int getNewConnectionId() {
        highestSocketId += 1;
        return highestSocketId;
    }

    public boolean addSubscriptionConnection(int connectionId, SubscriptionConnection subscriptionConnection) {
        for (Resource resource : resources.values()) {
            subscriptionConnection.addToIncomingResources(resource);
        }
        subscriptionConnections.put(connectionId, subscriptionConnection);
        return true;
    }

    public boolean removeSubscriptionConnection(int connectionId) {
        return (subscriptionConnections.remove(connectionId) != null);
    }

    public Resource findResource(Resource resource) {
        return resources.get(resource.getPrimaryKey());
    }

    // May falsely say a remove failed because the resource did not exist if another
    // thread inserts a resource with the same primary key during this method's
    // execution, but this behaviour shouldn't be unexpected because the outcome
    // will be the same, as if this function came first in the sequence.
    // Conversely, a remove could say it succeeded but then be added again from another
    // thread before the function returns, however this just means that the system
    // behaves as if the other thread came second in the sequence.
    public boolean removeResource(Resource resource) {
        String primaryKey = resource.getPrimaryKey();
        // Another thread could insert a resource with the same primary key while
        // the != null check is taking place
        return (resources.remove(primaryKey) != null);
    }

    public String getAllAsString() {
        StringBuilder output = new StringBuilder();
        for (Map.Entry<String,Resource> entry : resources.entrySet()) {
            output.append(entry.getValue().toJson().toString()); output.append("\n");
        }
        return output.toString();
    }

    public void printAllResources() {
        Logger.log(LogType.DEBUG, getAllAsString());
    }
    
    public ArrayList<Resource> findResourceByTemplate(Resource resourceTemplate) {
        // We don't synchronize on resources. Iterators returned by ConcurrentHashMaps
        // are thread-safe as long as we don't pass them off to other Threads. Another
        // thread may delete a resource while we're iterating, leading to answering
        // them with a list of resources that have already been deleted, but a Client
        // should expect that the list of resources on the Server might have changed
        // by the time they receive a response anyway.
    	ArrayList<Resource> output = new ArrayList<Resource>();
        for (Map.Entry<String,Resource> entry : resources.entrySet()) {
            Resource resource = entry.getValue();
            if (resource.matchesResourceTemplate(resourceTemplate)) {
                output.add(resource);
            }
        }
        return output;
    }
}
