/**
 * <h2>Resource Container</h2>
 * <p>The Resource Container class let the EZShare.client.server can store the resources.</p>
 * <p>So the EZShare.client.server an add, search, remove, print the resources by using the Resource Container class</p>
 * 
 * @author Group 88888
 * @version 1.0
 * 
 */
package EZShare.server;

import EZShare.common.Resource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//TODO this is a rough draft, double check is OK and replace references to resources for subscribption where appropriate
public class SubscriptionContainer {

    private List<SubscriptionConnection> subscriptions = Collections.synchronizedList(new ArrayList<>());

    public SubscriptionContainer() {}

    public boolean addSubscription(SubscriptionConnection server) {

        subscriptions.add(server);
        return true;
    }
    public SubscriptionConnection findSubscription(String id) {
    	SubscriptionConnection s1=null;
    //	for(SubscriptionConnection s:subscriptions)
    //	{ if(s.getUniqueID()==id){
    //   s1=s;
    //    }}
    	return s1;
    }


    // TODO: Make this return a clone
    public List<SubscriptionConnection> getSubscriptions(){
        // Returns all severs
        return subscriptions;
    }

//    public boolean removeServerAddress(SubscriptionConnection inputServerList) {
//        int foundIndex = -1;
//        synchronized (subscriptions) {
//            for (int i = 0; i < subscriptions.size(); i++) {
//                if (subscriptions.get(i).serverMatches(inputServerList)) {
//                    foundIndex = i;
//                    break;
//                }
//            }
//            if (foundIndex > -1) {
//                servers.remove(foundIndex);
//            }
//        }
//        return foundIndex > -1;
//    }

//

    public void notifyNewResource(Resource newResource) {
        //TODO i think there's a bug here
        //TODO you need to finish off these functions
        //for (SubscriptionConnection subscription : subscriptions ) {
        //    subscription.checkNewResource(newResource);
        //}
    }


}
