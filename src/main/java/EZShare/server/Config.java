/**
 * <h2>Config</h2>
 * <p>In the config class, we define the init value for EZShare.client.server configurations.</p>
 * <p>People can use getOption() to get the configurations of a EZShare.client.server.</p>
 * 
 * @author Group 88888
 * @version 1.0
 * 
 */
package EZShare.server;

import java.net.InetAddress;
import java.net.UnknownHostException;
import EZShare.common.ServerAddress;

import org.apache.commons.cli.*;

public class Config {

    private String advertisedHostname = "localhost";
    private int connectionIntervalLimit = 1;
    private int exchangeInterval = 600;
    private int port = 3000;
    private int SSLPort = 3781;
    private String secret = "wZC4uSaXE2N3ESY6cPmiketd";
    private boolean debug = false;


    public Config(String[] args) throws ParseException {
    	// Set default advertisedHostname
    	try {
			advertisedHostname = InetAddress.getLocalHost().getHostAddress().toString();
		} catch (UnknownHostException e) {
			advertisedHostname = "localhost";
			System.out.println("Host error in server configuration");
		}

        Options options = getOptions();
        CommandLineParser parser = new DefaultParser();
        CommandLine commandLine = parser.parse(options, args);

        // Change the default port if using SSL; this will be overridden by
        // a supplied port argument

        advertisedHostname = commandLine.hasOption("advertisedhostname") ?
                commandLine.getOptionValue("advertisedhostname") :
                advertisedHostname;

        connectionIntervalLimit = commandLine.hasOption("connectionintervallimit") ?
                Integer.parseInt(commandLine.getOptionValue("connectionintervallimit")) :
                connectionIntervalLimit;

        exchangeInterval = commandLine.hasOption("exchangeinterval") ?
                Integer.parseInt(commandLine.getOptionValue("exchangeinterval")) :
                exchangeInterval;

        port = commandLine.hasOption("port") ?
                Integer.parseInt(commandLine.getOptionValue("port")) :
                port;

        SSLPort = commandLine.hasOption("sport") ?
                Integer.parseInt(commandLine.getOptionValue("sport")) :
                SSLPort;

        secret = commandLine.hasOption("secret") ?
                commandLine.getOptionValue("secret") :
                secret;

        debug = commandLine.hasOption("debug");
    }

    public static Options getOptions() {
        Options options = new Options();
        options.addOption("advertisedhostname", true, "advertised hostname");
        options.addOption("connectionintervallimit", true, "connection interval limit in seconds");
        options.addOption("exchangeinterval", true, "exchange interval in seconds");
        options.addOption("port", true, "port, an integer");
        options.addOption("sport", true, "use SSL encryption");
        options.addOption("secret", true, "secret");
        options.addOption("debug", false, "print debug information");
        return options;
    }

    public String getAdvertisedHostname() {
    	return advertisedHostname;
    }
    
    public int getConnectionIntervalLimit() {
    	return connectionIntervalLimit;
    }

    public int getSSLPort() { return SSLPort;}

    public int getExchangeInterval() {
    	return exchangeInterval;
    }
    
    public int getPort() {
    	return port;
    }
    
    public String getSecret() {
    	return secret;
    }
    
    public boolean isDebug() {
    	return debug;
    }
    
    public String getServerDetails(){
    	return this.getAdvertisedHostname()+":"+Integer.toString(this.getPort());
    }

    public ServerAddress getServerAddress(boolean isSSL) {
        if(isSSL){
			return new ServerAddress(getAdvertisedHostname(), getPort());
		}
		else {
        	return new ServerAddress(getAdvertisedHostname(), getSSLPort());
		}
    }

}
