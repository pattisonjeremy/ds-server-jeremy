package EZShare.server;
import EZShare.common.CommandType;
import EZShare.common.ServerAddress;
import EZShare.common.ServerCommand;
import EZShare.logger.LogType;
import EZShare.logger.Logger;
import EZShare.Server;

/**
 * Created by Jeremy on 28/04/2017.
 */

// For EXCHANGE with other servers
public class ServerFunction implements Runnable {
    private ServerAddressContainer serverList;
    private long timeWait;
    private Config configuration;
    private boolean isSSL;

    public ServerFunction(ServerAddressContainer newServers, Config newConfiguration, boolean newIsSSL) {
        serverList = newServers;
        configuration = newConfiguration;
        timeWait = configuration.getExchangeInterval() * 1000;
        isSSL = newIsSSL;
    }

    public void run() {
        long timeEnd = System.currentTimeMillis() + timeWait;
        long timeToSleep = Math.min(timeWait, 30);
        ServerAddress targetServer;
        ServerCommand serverCommand;

        while (true) {
            try {
                Thread.sleep(timeToSleep);
            } catch (InterruptedException e) {
                Logger.log(LogType.INFO, "Interrupted Exception Detected");
                timeEnd = System.currentTimeMillis() + timeWait;
                return;
            }
            if (System.currentTimeMillis() > timeEnd) {
                timeEnd = System.currentTimeMillis() + timeWait;
                targetServer = serverList.getRandom();

                if (targetServer == null) {
                    continue;
                }
                if (targetServer.getHostname() == configuration.getAdvertisedHostname()) {
                    continue;
                }
                serverCommand = new ServerCommand(CommandType.EXCHANGE, serverList.getServers(), configuration);
                Server.exchangeServer(targetServer, serverCommand, serverList, isSSL);
            }
        }
    }
}
