package EZShare.server;

import EZShare.logger.LogType;
import EZShare.logger.Logger;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Jeremy on 10/05/2017.
 *
 * Note it might be possible to merge this with the serverResponseUnesecured class (youll need to run 2 threads though)although probs do at end in case it gets messy
 */
public class ServerDispatcher implements Runnable {
    private Config config;
    private ResourceContainer resources;
    private ServerAddressContainer servers;
    private ConcurrentHashMap<InetAddress, Long> serverTimes;
    private boolean isSSL;
    private int port;
    private ConcurrentHashMap<Integer,SubscriptionConnection> subscriptions;
    public ServerDispatcher(Config newConfig, ResourceContainer newResources, ServerAddressContainer newServers, ConcurrentHashMap<InetAddress, Long> newMap, ConcurrentHashMap<Integer,SubscriptionConnection> newSubscriptions, boolean newIsSSL) {
        config = newConfig;
        resources = newResources;
        servers = newServers;
        serverTimes = newMap;
        isSSL = newIsSSL;
        subscriptions = newSubscriptions;
    }

    public void run() {
        InetAddress newAddress;
        int intervalTime = config.getConnectionIntervalLimit() * 1000; // x1000 to convert to milliseconds
        if (isSSL) {
            port = config.getSSLPort();
            Logger.log(LogType.INFO, "Secured server connection set up at port " + port);
        }
        else {
            port = config.getPort();
            Logger.log(LogType.INFO, "unsecured server connection set up at port " + port);
        }

        try {
            ServerSocket serverSocket;
            if (isSSL) {
                SSLServerSocketFactory factory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
                serverSocket = (SSLServerSocket) factory.createServerSocket(port);
            }
            else {
                ServerSocketFactory factory = ServerSocketFactory.getDefault();
                serverSocket = factory.createServerSocket(port);
            }
            while (true) {
                Socket clientSocket = serverSocket.accept();//someone connect to the EZShare.client.server
                newAddress = clientSocket.getInetAddress();
                if (serverTimes.containsKey(newAddress) && serverTimes.get(newAddress) > System.currentTimeMillis() - intervalTime) {
                    Logger.log(LogType.INFO, "Blocked a request from " + newAddress.toString());
                    continue;
                }
                Logger.log(LogType.INFO, "Accepting a request from " + newAddress.toString());
                serverTimes.put(newAddress, System.currentTimeMillis()); // record when they joined
                // Spawn a new thread to take care of the request
                new Thread(new ServerRunnable(clientSocket, config, resources, servers, this.isSSL, subscriptions)).start();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
