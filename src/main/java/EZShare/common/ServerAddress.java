package EZShare.common;

import EZShare.server.SubscriptionConnection;
import org.json.simple.JSONObject;

import java.util.concurrent.ConcurrentHashMap;


public class ServerAddress {
    String hostname = "";
    int port = -1;
    boolean isSSL;
    private ClientSubscribe subscriber;

    public ServerAddress(String newHostname, int newPort) {
        hostname = newHostname;
        port = newPort;
    }

    public void addSubscription(Resource template, ConcurrentHashMap<Integer,SubscriptionConnection> subscriptions) {
        //either creates or adds the template to the current connection
        ClientCommand command = new ClientCommand(CommandType.SUBSCRIBE, this, template, null, false, this.isSSL);
        ClientConnection connection;
        if (subscriber==null) {
            connection = new ClientConnection(command, true);
            subscriber = connection.getSubscription();
        }
        else {
            subscriber.sendCommand(command);
        }

    }

    public ServerAddress(JSONObject jsonServer) {
        String jsonHostname = (String) jsonServer.get("hostname");
        Long jsonPort = (Long) jsonServer.get("port");
        if (jsonHostname != null) {
            hostname = jsonHostname;
        }
        if (jsonPort != null && jsonPort > -1) {
            port = jsonPort.intValue();
        }
    }

    public ServerAddress(String serverString) {
        String[] hostPort = serverString.split(":");
        if (hostPort.length == 2) {
            hostname = hostPort[0];
            try {
                port = Integer.parseInt(hostPort[1]);
            } catch (NumberFormatException e) {
                port = -1;
            }
        }
    }

    public String getHostname() {
        return hostname;
    }

    public int getPort() {
        return port;
    }

    public boolean isValid() {
        return (!hostname.equals("") && port != -1);
    }

    public String toString() {
        return (hostname + ":" + port);
    }

	public boolean serverMatches(ServerAddress templateServerAddress) {
		return templateServerAddress.getHostname().equals(hostname);
	}
	
	public JSONObject toJson() {
		JSONObject output = new JSONObject();
		output.put("hostname", this.hostname);
		output.put("port", this.port);
		return output;
	}

	public boolean equals(ServerAddress other) {
        return getHostname().equals(other.getHostname()) && getPort() == other.getPort();
    }
}
