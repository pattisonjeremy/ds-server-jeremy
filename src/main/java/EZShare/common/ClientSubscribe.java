package EZShare.common;

import EZShare.Client;
import EZShare.logger.LogType;
import EZShare.logger.Logger;
import EZShare.server.SubscriptionConnection;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Scanner;

/**
 * Created by Jeremy on 27/05/2017.
 */
public class ClientSubscribe  implements Runnable {
	private DataOutputStream output;
	private DataInputStream input;
	private boolean isClient = false;
	private JSONParser parser = new JSONParser();
	//private ConcurrentHashMap<Integer,SubscriptionConnection> subscriptions;

	public ClientSubscribe(DataInputStream input, DataOutputStream output, boolean clientValue) {
		this.input = input;
		this.output = output;
		this.isClient = clientValue;
		//this.subscriptions = subscriptions;
	}


	/*
	Potential bugs:

	what if two subscriptions of very similar things (i.e. subscription everything, and one for name john. do two occur?

	 */
	public void run() {
		JSONObject inputJson;
		Resource serverResource = null;
		String message;
		String outputMessage;
		while (true) {
			Logger.log(LogType.INFO,"ClientSubscribe is listening for new messages");
			message = ClientConnection.receiveMessage(input);
			System.out.println(message);

			try {
				// make sure we maintain the persistent connection
				inputJson = (JSONObject) parser.parse(message);
				if (inputJson.get("resultSize") == null) {
					serverResource = new Resource(inputJson);
					serverResource.setRelayValue(true); // indicates that the file came from a server
					outputMessage = serverResource.toString();
				}
				else {
					int resultSize = ((Long) inputJson.get("resultSize")).intValue();
					outputMessage = String.format("%d messages received", resultSize);
					if (isClient) { Logger.log(LogType.RECEIVED, outputMessage); }
					break;
				}
			} catch (ParseException e) {
				//e.printStackTrace();
				outputMessage = e.getMessage();
			}
			// error handling???

			if (isClient) {
				Logger.log(LogType.INFO, "Client Subscribe is receiving a message");
				Logger.log(LogType.RECEIVED, outputMessage);

			}
			//else {
			//	//if(serverResource!=null){
			//	//	for (SubscriptionConnection subscriptionConnection : subscriptions.values()) {
			//	//		//TODO check for ssh properly
			//	//		subscriptionConnection.addToIncomingResources(serverResource);
			//	//	}
			//	//}


			//	/* If its a resource:
			//		iterate through all subscriptions and put the template in

			//		One thing to be careful of is if this is an ssh connection only go to subscriptions that are ssh and vice versa
			//		i think each subscription is either ssh or not
			//	 */
			//}
		}
	}

	public void sendCommand(ClientCommand command) {
		// we send command for either subscribe or unsubscribe
		// handling the respond is done by the 'run' function
		String message = command.toJson().toJSONString();
		try {
			Logger.log(LogType.SENT, message);
			output.writeUTF(message);
			output.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void sendUnsubscribe(String id) throws IOException {
	    JSONObject unsub = new JSONObject();
	    unsub.put("command", "UNSUBSCRIBE");
	    unsub.put("id", id);
	    output.writeUTF(unsub.toJSONString());
	    output.flush();
	}


}
