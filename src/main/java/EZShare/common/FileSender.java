package EZShare.common;
// This is a EZShare.client.common use function for sending files

import EZShare.logger.LogType;
import EZShare.logger.Logger;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.SocketException;
import java.net.URI;
import java.util.Arrays;

public class FileSender{
  private URI fileLocation;  // URI of file to send
  private File fileToSend;
  private int fileSize;
  private static final int maxPacketSize = 1024*1024;
  
  public FileSender(URI fileUri) {
	  fileLocation = fileUri;
	  fileToSend = new File (fileLocation);
	  if (fileToSend.exists()) {
		  fileSize = ((int) fileToSend.length()); // TODO need to upgrade code to handle file size long
		  Logger.log(LogType.INFO, "Sending file");
	  }
	  else {
		  Logger.log(LogType.ERROR, "Could not find file " + fileUri.toString());
	  }
  }
  
  public void sendFile(DataOutputStream output) throws FileNotFoundException {
	  RandomAccessFile byteFile = new RandomAccessFile(fileLocation.getRawPath(), "r");
	  try {
		  // send as segments
		  byte[] sendingBuffer = new byte[maxPacketSize];
		  int num;
		  while((num = byteFile.read(sendingBuffer)) > 0) {
			  // send data
			  output.write(Arrays.copyOf(sendingBuffer, num));
		  }
		  byteFile.close();
		  Logger.log(LogType.SENT, fileLocation.toString() + ", " + fileSize + " bytes");
	  } catch (SocketException e) {
	      Logger.log(LogType.ERROR, "Connection with client broken during file transfer");
	  } catch (IOException e) {
		  e.printStackTrace();
	  }
  }

  public int getFileSize() {
	  return fileSize;
  }
    
  public void setFileLocation(URI location) {
	  fileLocation = location;
  }
  
  public URI getFileLocatoin() {
	  return fileLocation;
  }
}