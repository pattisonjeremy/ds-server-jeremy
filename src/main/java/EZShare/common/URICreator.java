package EZShare.common;

import java.net.URI;
import java.net.URISyntaxException;

public class URICreator {
    // This function exists because creating a URI could throw two different kinds of
    // exception, URISyntaxException and IllegalArgumentException, and really they
    // mean the same thing
    public static URI createUri(String string) throws URISyntaxException {
        try {
            return new URI(string);
        } catch (IllegalArgumentException e) {
            throw new URISyntaxException(string, "URI not absolute");
        }
    }
}
