package EZShare.common;
import EZShare.server.Config;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Overall class for dealing with the type of command when running from EZShare.Server, gathers relevant information via JSONObject
public class ServerCommand extends Command {

    private Config config;

    public ServerCommand(String jsonString, Config configuration) throws org.json.simple.parser.ParseException {
        this((JSONObject) new JSONParser().parse(jsonString), configuration);
    }
    public ServerCommand(CommandType newCommandType, List<ServerAddress> serverList, Config newConfig) {
        commandType = newCommandType;
        config = newConfig;
        for (ServerAddress server : serverList) {
            addServer(server);
        }
    }

    public ServerCommand(JSONObject jsonCommand, Config newConfig) {
        setConfig(newConfig);

        if (jsonCommand.get("command") != null) {
            switch((String) jsonCommand.get("command")) {
                //get the ""command" of the resource, and switch it in different cases
                case "EXCHANGE":
                    commandType = CommandType.EXCHANGE;
                    break;
                case "FETCH":
                    commandType = CommandType.FETCH;
                    break;
                case "SUBSCRIBE":
                	commandType = CommandType.SUBSCRIBE;
                    break;
                case "UNSUBSCRIBE":    
                    commandType = CommandType.UNSUBSCRIBE;
                    break;
                    
                case "PUBLISH":
                    commandType = CommandType.PUBLISH;
                    break;
                case "QUERY":
                    commandType = CommandType.QUERY;
                    break;
                case "REMOVE":
                    commandType = CommandType.REMOVE;
                    break;
                case "SHARE":
                    commandType = CommandType.SHARE;
                    break;
                default:
                    commandType = CommandType.INVALID;
                    break;
            }
        } else {
            commandType = CommandType.MISSING;
        }

        // We need to get a resource either out of key "resource" or "resourceTemplate" depending on the
        // command type
        String resourceString = "resource";
        String ezServer = null;
        if (commandType == CommandType.QUERY || commandType == CommandType.FETCH || commandType == CommandType.SUBSCRIBE) {
            resourceString = "resourceTemplate";
        }
        // Grab the ezServer before we create the Resource so we can give it
        if (commandType == CommandType.PUBLISH || commandType == CommandType.SHARE) {
            ezServer = config.getServerDetails(); //TODO change this
        }
        // Now we look for "resource" or "resourceTemplate" and convert it to a Resource object
        JSONObject jsonResource = (JSONObject) jsonCommand.get(resourceString);
        if (jsonResource != null) {
            setResource(new Resource(jsonResource, ezServer));
        } else {
            setResource(null);
        }

        if (jsonCommand.get("id") != null) {
            try {
            setId((String) jsonCommand.get("id"));
            } catch (ClassCastException e) {
                setIdHasTypeError(true);
            }
        }

        if (jsonCommand.get("serverList") != null) {
            try {
                JSONArray serverArray = (JSONArray) jsonCommand.get("serverList");
                Iterator<JSONObject> iterator = serverArray.iterator();
                ArrayList<String> serverStringList = new ArrayList<>();
                try {
                    while (iterator.hasNext()) {
                        JSONObject serverObject = iterator.next();
                        //TODO we're assuming correct message
                        addServer(serverObject);
                    }
                } catch (ClassCastException e) {
                }
            } catch (ClassCastException e) {
                // if serverList isn't a list
                nullifyServers();
            }
        } else {
            setServers((String[]) null);
        }
        setSecret(null);
        if (jsonCommand.get("secret") != null) {
            setSecret((String) jsonCommand.get("secret"));
        }
    }

    public boolean hasSecret() {
        return getSecret() != null;
    }
    
    public void setConfig(Config newConfig){
    	this.config = newConfig;
    }

}
