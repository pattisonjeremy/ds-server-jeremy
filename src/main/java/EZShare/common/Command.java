package EZShare.common;

import org.json.simple.JSONObject;

import java.util.ArrayList;

public class Command {
    //Define the variables in the command
    private String secret = "wZC4uSaXE2N3ESY6cPmiketd"; //TODO remove default secret
    protected ArrayList<ServerAddress> servers = new ArrayList<>();
    public Boolean debug = false;
    public CommandType commandType = CommandType.INVALID;
    private Resource resource;
    public boolean relay = true;
    private String id ="";
    private boolean idHasTypeError = false;
    // Read or Reset the variables.

    public String getSecret() {
        return secret;
    }
    
    public void setId(String newId) {
    	id = newId;
    }
    
    public String getId() {
    	return id;
    }

    public void setSecret(String newSecret) {
        secret = newSecret;
    }

    public ArrayList<ServerAddress> getServers() {
        return servers;
    }

    public void nullifyServers() {
        servers = null;
    }
    
    public void addServer(ServerAddress server) {
        if (server != null) {
            servers.add(server);
        }
    }

    public void addServer(String message) {
    	// When message input is form hostname:port
        servers.add(new ServerAddress(message));
    }

    public void addServer(JSONObject jsonServer) {
        ServerAddress newServer = new ServerAddress(jsonServer);
        // Add even if not valid, i.e. don't do isValid check
        addServer(newServer);
    }

    public void setServers(ServerAddress[] newServerList) {
        if (newServerList == null) {
            return;
        }
        servers.clear();
        for (ServerAddress server : newServerList) {
            addServer(server);
        }
    }
    
    public void setRelay(boolean value) {
        this.relay = value;
    }

    public Boolean getRelay(){
        return this.relay;
    }

    public void setServers(String[] serverStringList) {
        if (serverStringList == null) {
            servers = null;
            return;
        }
        servers.clear();
        for (String serverString : serverStringList) {
            ServerAddress serverAddress = new ServerAddress(serverString);
            servers.add(serverAddress);
        }
    }

    public Resource getResource() {
        return resource;
    }
    
    public void setResource(Resource newResource) {
        resource = newResource;
    }
    public Resource setResource2(Resource newResource) {
        resource = newResource;
        return resource;
    }

    public Boolean isDebug() {
        return debug;
    }

    public void setDebug(Boolean value) {
        debug = value;
    }

    public CommandType getCommandType() {
        return commandType;
    }

    public boolean hasSecret() {
        return getSecret() != null;
    }

    public boolean hasInvalidServerInList() {
        if (getServers() == null) {
            return false;
        }
        for (ServerAddress serverAddress : getServers()) {
            if (!serverAddress.isValid()) {
                return true;
            }
        }
        return false;
    }

    public boolean idHasTypeError() {
        return idHasTypeError;
    }

    protected void setIdHasTypeError(boolean value) {
        idHasTypeError = value;
    }

}
