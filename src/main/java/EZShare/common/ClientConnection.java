package EZShare.common;

import java.io.*;
import java.net.Socket;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import EZShare.server.SubscriptionConnection;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import EZShare.logger.LogType;
import EZShare.logger.Logger;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class ClientConnection {
	private String host;
	private int port;
	private ClientCommand command;
	private DataInputStream input;
	private DataOutputStream output;
	private JSONParser parser = new JSONParser();
	private JSONObject initialResponse = new JSONObject();

	private ArrayList<Resource> resourceList = new ArrayList<>();
	private JSONObject resultSizeMessage;
	private boolean isSSL;
	private ClientSubscribe clientSubscription;
	private boolean isClient;
	Socket socket;

	public ClientConnection(ClientCommand newCommand, boolean isClient) {
		command = newCommand;
		host = command.getHost();
		port = command.getPort();
		isSSL = command.isSSL();
		this.isClient = isClient;
		execute();
	}

	// Perform communication
	private void execute() {
		try {
			if (isSSL) {
				SSLSocketFactory sslSocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
				socket = (SSLSocket) sslSocketfactory.createSocket(host, port);
			} else {
				SocketFactory socketFactory = SocketFactory.getDefault();
				socket = (Socket) socketFactory.createSocket(host, port);
			}
			// Output and Input Stream
			input = new DataInputStream(socket.
					getInputStream());
			output = new DataOutputStream(socket.
					getOutputStream());
			Logger.log(LogType.INFO, String.format((isSSL ? "Secured" : "Unsecured") + " connection made with port: %d and hostname: %s", command.getPort(), command.getHost()));
			JSONObject out = command.toJson();

			// string representation of json message
			String jsonText = out.toJSONString();

			output.writeUTF(jsonText);
			output.flush();
			Logger.log(LogType.SENT, String.format("%s", jsonText));
			// Message for input
			String message;

			// The JSON Parser

			message = receiveMessage(input);
			try {
				initialResponse = (JSONObject) parser.parse(message);
			} catch (NullPointerException e) {
				Logger.log(LogType.ERROR, "Did not receive response");
				return;
			}

			if (initialResponse == null) {
				// Error
				Logger.log(LogType.ERROR, "No response found");
				return;
			}
			// Handle commands which require multiple messages
			if (responseSuccessful()) {
				switch (command.getCommandType()) {
					case FETCH:
						try {
							readFetchResults();
						} catch (FileNotFoundException e) {
                            Logger.log(LogType.ERROR, "Could not open file for receiving");
						} catch (URISyntaxException e) {
							Logger.log(LogType.ERROR, "Returned resource has invalid URI");
						} catch (ParseException e) {
						    Logger.log(LogType.ERROR, "Invalid JSON in response");
						}
						break;
					case QUERY:
						
						try {
							readQueryResults();
						} catch (ParseException e) {
							Logger.log(LogType.ERROR, "Invalid JSON in response");
						}
						break;
						
					case SUBSCRIBE:
					    Logger.log(LogType.INFO, "Start Client Subscribe object to listen");

						this.clientSubscription = new ClientSubscribe(input, output, isClient); //wrong
						new Thread(clientSubscription).start();
						break;
					case UNSUBSCRIBE:
						try {
							readUnsubscribeResults();
						} catch  (ParseException e) {
							Logger.log(LogType.ERROR, "Invalid JSON in response");
						}
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Logger.log(LogType.ERROR, "Could not open socket");
			initialResponse.put("response", "error");
			initialResponse.put("errorMessage", e.getMessage());
		} catch (ParseException e) {
			initialResponse.put("response", "error");
			initialResponse.put("errorMessage", e.getMessage());
		}
	}

	public ClientSubscribe getSubscription() {
		return this.clientSubscription;
	}
	private void readQueryResults() throws ParseException {
		// Loop through reading resource
		Logger.log(LogType.DEBUG, "Reading query results");
		String message = receiveMessage(input);
		JSONObject inputJson = (JSONObject) parser.parse(message);
		Resource serverResource;
		while ((inputJson.get("resultSize") == null)) {
			Logger.log(LogType.DEBUG, "Reading a resource");
			// Print out each resource
			serverResource = new Resource(inputJson);
			resourceList.add(serverResource);
			message = receiveMessage(input);
			inputJson = (JSONObject) parser.parse(message);
		}
	}
	
	private void readSubscribeResults() throws ParseException {
		// Loop through reading resource
		String message = receiveMessage(input);
		JSONObject inputJson = (JSONObject) parser.parse(message);
		Resource serverResource;
		while ((inputJson.get("resultSize") == null)) {
			// Print out each resource
			serverResource = new Resource(inputJson);
			resourceList.add(serverResource);
			message = receiveMessage(input);
			inputJson = (JSONObject) parser.parse(message);
		}
	}
	
	private void readUnsubscribeResults() throws ParseException {
		// Loop through reading resource
		String message = receiveMessage(input);
		JSONObject inputJson = (JSONObject) parser.parse(message);
		Resource serverResource;
		while ((inputJson.get("resultSize") == null)) {
			// Print out each resource
			serverResource = new Resource(inputJson);
			resourceList.add(serverResource);
			message = receiveMessage(input);
			inputJson = (JSONObject) parser.parse(message);
		}
	}

	private void readFetchResults() throws URISyntaxException, FileNotFoundException, ParseException {
		// loop through reading resource
		String message = receiveMessage(input);
		JSONObject jsonResource = (JSONObject) parser.parse(message);
		FileReceiver fileTransporter;

		while ((jsonResource.get("resultSize") == null)) {
			Resource serverResource = new Resource(jsonResource);
			fileTransporter = new FileReceiver(serverResource);
			Logger.log(LogType.INFO, "Putting received file in " + fileTransporter.getFileLocation());
			fileTransporter.receiveFile(input);
			Logger.log(LogType.RECEIVED, "Received " + serverResource.getReceivedFilename());
			//TODO ensure that we aren't meant to be using ezServer which is provided for fetch
			message = receiveMessage(input);
			jsonResource = (JSONObject) parser.parse(message);
		}
		this.resultSizeMessage = jsonResource;
	}

	public JSONObject getInitialResponse() {
		return initialResponse;
	}

	// Function to read and respond to first generic first message received from server
	public static boolean responseSuccessful(JSONObject jsonResponse) {
		if (jsonResponse == null) {
			return false;
		}

		if (jsonResponse.get("response") != null) {
			String status = jsonResponse.get("response").toString();
			if (status.equals("success")) {
				return true;
			} else if (status.equals("error")) {
				return false;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public boolean responseSuccessful() {
	    return responseSuccessful(getInitialResponse());
	}

	public static String receiveMessage(DataInputStream input) {
		// Reads a message from server, waits total of 5 seconds to receive message

		String message = null;
		long endTime = System.currentTimeMillis() + 5000;
		boolean receivedAnything = false;

		try {
			while (System.currentTimeMillis() < endTime) {
				//
				message = input.readUTF();
				if (message != null) {
					receivedAnything = true;
					break;
				} else {
					Thread.sleep(100);
				}
			}
			if (receivedAnything) {
				Logger.log(LogType.RECEIVED, String.format("%s", message));
			} else {
				Logger.log(LogType.INFO, "Did not receive a timely response");
			}
		} catch (IOException e) {
		    Logger.log(LogType.ERROR, "IO error reading from socket");
		} catch (InterruptedException e) {
		    Logger.log(LogType.ERROR, "Socket polling interrupted while trying to sleep");
		}
		return message;
	}

	public ArrayList<Resource> getResourceList() {
		return resourceList;

	}
}
