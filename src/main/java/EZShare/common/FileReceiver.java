package EZShare.common;
// This is a EZShare.client.common use function for sending files

import EZShare.logger.LogType;
import EZShare.logger.Logger;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

public class FileReceiver {
    private String fileLocation;  // URI of file to send
    private int fileSize;
    private static final int MAXIMUM_CHUNK_SIZE = 1024 * 1024;

    public FileReceiver(Resource resource) throws URISyntaxException {
    	URI fileUri = URICreator.createUri(getReceivedFilePath(resource));
        setFileLocation(fileUri.getRawPath());
        setFileSize(resource.getResourceSize());
	}
  
    public Boolean receiveFile(DataInputStream input) throws FileNotFoundException {
  	  // Download file. Return true if successfully received. False if not.
  	    RandomAccessFile downloadingFile = new RandomAccessFile(fileLocation, "rw");
  	    // Find out how much size is remaining to get from EZShare.client.server
  	    long fileSizeRemaining = (long) fileSize;

  	    int chunkSize = setChunkSize(fileSizeRemaining);

  	    // Receiving buffer
  	    byte[] receiveBuffer = new byte[chunkSize];

  	    // Variable used to read if there are remaining size left to read.
  	    int num;

  	    try {
  	  	    while ((num = input.read(receiveBuffer)) > 0) {
  	  	  	    //write received bytes into randomAccessFile
				downloadingFile.write(Arrays.copyOf(receiveBuffer, num));

  	  	    	  // Reduce file size left to read
				fileSizeRemaining -= num;

				//set chunkSize again
				chunkSize = setChunkSize(fileSizeRemaining);
				receiveBuffer = new byte[chunkSize];

  	  	    	  // if done then break
				if (fileSizeRemaining == 0) {
					break;
				}
  	  	    }
  	  	    downloadingFile.close();
  	  	    return true;
  	    } catch (IOException e) {
            Logger.log(LogType.ERROR, "IO exception while receiving file");
	  	    return false;
	    }
    }

	public static int setChunkSize(long fileSizeRemaining){
		// If the file size remaining is less than the chunk size
		// then set the chunk size to be equal to the file size;
		// else return the maximum chunk size
		if (fileSizeRemaining < MAXIMUM_CHUNK_SIZE) {
			return (int) fileSizeRemaining;
		} else {
			return MAXIMUM_CHUNK_SIZE;
		}
	}

    private void setFileLocation(String location) {
	  fileLocation = location;
  }
  
    public String getFileLocation() {
	  return fileLocation;
  }

    private void setFileSize(int newFileSize) {
		fileSize = newFileSize;
	}

	public int getFileSize() {
		return fileSize;
	}

	private String getReceivedFilePath(Resource resource) throws URISyntaxException {
		String saveDirectory = System.getProperty("user.dir");
		String OS = System.getProperty("os.name").toLowerCase();
		if (OS != null && OS.contains("windows")) {
			saveDirectory = saveDirectory.replace(File.separatorChar, '/');
		}
		return "file:///" + saveDirectory + "/" + resource.getReceivedFilename();
	}
}