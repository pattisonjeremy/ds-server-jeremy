/**
 * 
 */
package EZShare.common;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import EZShare.logger.LogType;
import EZShare.logger.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

// The Resource class can represent a valid or invalid resource.
// Resources can be created from any valid JSON or CLI arguments.
// All of their members are always defined (not null), and where
// a member (e.g. "description") is not supplied it will become a
// non-null empty value. The validity of a Resource's URI MUST be
// checked by one of the validation methods available.
// Where specific restrictions apply (e.g. PUBLISH Resources must not
// point to a file) they must also be validated. Invalid fields
// such as " a\0b " will be automatically corrected (to "ab").
// However, where the Resource was parsed from a JSONObject which
// contained type errors, such as {"description": []}, it will
// remember this with a flag and fail validation methods.
// RESOURCES ARE MEANT TO BE IMMUTABLE!
public class Resource {
	// Set the default values; nothing can be null
	private String name = "";
	private String description = "";
	public ArrayList<String> tags = new ArrayList<>();
	private String uri = "";
	private String channel = "";
	private String owner = "";
	private String ezServer = "";
	private int resourceSize = -1;
	private boolean containsTypeErrors = false;
	private String subscriptionId = "";
	private boolean isRelay = false; // For subscription templates
	private boolean isSSH = false;

	// CONSTRUCTORS

	public Resource(String newName, String newDescription, String newTags, String newUri, String newChannel, String newOwner, String newEzServer) {
	    setName(newName);
		setDescription(newDescription);
		setTags(newTags);
		setUri(newUri);
		setChannel(newChannel);
		setOwner(newOwner);
		setEzServer(newEzServer);
	}

	public Resource(String newName, String newDescription, ArrayList<String> newTags, String newUri, String newChannel, String newOwner, String newEZserver) {
		setName(newName);
		setDescription(newDescription);
		setTags(newTags);
		setUri(newUri);
		setChannel(newChannel);
		setOwner(newOwner);
		setEzServer(newEZserver);
	}

	public Resource(String newName, String newDescription, ArrayList<String> newTags, String newUri, String newChannel, String newOwner, String newEZserver, int newResourceSize, String newSubscriptionId) {
		setName(newName);
		setDescription(newDescription);
		setTags(newTags);
		setUri(newUri);
		setChannel(newChannel);
		setOwner(newOwner);
		setEzServer(newEZserver);
		setResourceSize(newResourceSize);
		setSubscriptionId(newSubscriptionId);
	}

	public Resource(String newName, String newDescription, ArrayList<String> newTags, String newUri, String newChannel, String newOwner, String newEzServer, int newResourceSize) {
		this(newName, newDescription, newTags, newUri, newChannel, newOwner, newEzServer);
		setResourceSize(newResourceSize);
	}

	public Resource(Resource otherResource) {
		setName(otherResource.getName());
		setDescription(otherResource.getDescription());
		setUri(otherResource.getUri());
		setChannel(otherResource.getChannel());
		setOwner(otherResource.getOwner());
		setTags(otherResource.getTags());
		setEzServer(otherResource.getEzServer());
	}

	// Construct a Resource object from JSON
	public Resource(JSONObject jsonResource) {
		if (jsonResource.get("name") != null) {
			try {
				setName((String) jsonResource.get("name"));
			} catch (ClassCastException e) {
				containsTypeErrors = true;
			}
		}

		if (jsonResource.get("description") != null) {
			try {
				setDescription((String) jsonResource.get("description"));
			} catch (ClassCastException e) {
				containsTypeErrors = true;
			}
		}

		if (jsonResource.get("tags") != null) {
			try {
				setTags((JSONArray) jsonResource.get("tags"));
			} catch (ClassCastException e) {
				containsTypeErrors = true;
			}
		}

		if (jsonResource.get("uri") != null) {
			try {
				setUri((String) jsonResource.get("uri"));
			} catch (ClassCastException e) {
				containsTypeErrors = true;
			}
		}

		if (jsonResource.get("channel") != null) {
			try {
				setChannel((String) jsonResource.get("channel"));
			} catch (ClassCastException e) {
				containsTypeErrors = true;
			}
		}

		if (jsonResource.get("owner") != null) {
			try {
				setOwner((String) jsonResource.get("owner"));
			} catch (ClassCastException e) {
				containsTypeErrors = true;
			}
		}

		if (jsonResource.get("resourceSize") != null) {
			Logger.log(LogType.DEBUG, "Checking the resource size, reading as " + jsonResource.get("resourceSize") + " bytes");
			try {
				// Reason for cast to Long is that that's what JSONObject returns, and casting directly to
				// Integer gives incorrect values
				setResourceSize(((Long) jsonResource.get("resourceSize")).intValue());
			} catch (ClassCastException e) {
				containsTypeErrors = true;
			}
		}
    }

    // EzServer can't be derived from JSON, since it depends on whence the JSON is sent
	Resource(JSONObject jsonResource, String newEzServer){
		this(jsonResource);
		setEzServer(newEzServer);
	}

	// GETTERS AND SETTERS: ALL SETTERS SHOULD BE PRIVATE

	// Remove trailing and leading spaces as well as the character '\0' from the input
	private static String stripForbidden(String input) {
		if (input != null) {
			return input.replaceAll("(^ )|( $)", "").replace("\0", "");
		}
		return "";
	}

	private void setName(String newName) {
		name = stripForbidden(newName);
	}

	public String getName() {
		return name;
	}

	private void setDescription(String newDescription) {
		description = stripForbidden(newDescription);
	}

	public String getDescription() {
		return description;
	}

	public boolean getSshValue() { return this.isSSH;}
	public void setSSH(boolean sshValue){this.isSSH = sshValue;}

	private void setTags(String newTags) {
		if (newTags == null || newTags.isEmpty()) {
			tags = new ArrayList<>();
			return;
		}
		if (tags == null) {
			tags = new ArrayList<>();
		} else {
			tags.clear();
		}
		String[] newTagList = newTags.split(",");
		for (String newTag : newTagList) {
			tags.add(stripForbidden(newTag));
		}
	}

	// Note that JSONArray is an ArrayList, so this can be used safely with one
	private void setTags(ArrayList<String> newTagList) {
		if (newTagList == null) {
			tags = new ArrayList<>();
			return;
		}
		if (tags == null) {
		    tags = new ArrayList<>();
		} else {
			tags.clear();
		}
		for (String newTag : newTagList) {
			tags.add(stripForbidden(newTag));
		}
	}

	// This returns a reference to a mutable member variable so it's private
	public ArrayList<String> getTags() {
		return tags;
	}

	private void setUri(String newUri) {
		uri = stripForbidden(newUri);
	}

	public String getUri() {
		return uri;
	}

	private void setChannel(String newChannel) {
		channel = stripForbidden(newChannel);
	}

	public String getChannel() {
		return channel;
	}

	private void setOwner(String newOwner) {
		owner = stripForbidden(newOwner);
	}

	public String getOwner() {
		return owner;
	}

	private void setEzServer(String newEZserver) {
		ezServer = stripForbidden(newEZserver);
	}
	
	public String getEzServer() {
		return ezServer;
	}

	private void setResourceSize(int newResourceSize) {
	    resourceSize = newResourceSize;
    }

    public int getResourceSize() {
	    return resourceSize;
    }

    // PRIMARY KEY METHODS

	public String getPrimaryKey() {
		StringBuilder builder = new StringBuilder();
		builder.append(getOwner());
		builder.append('\0');
		builder.append(getChannel());
		builder.append('\0');
		builder.append(getUri());
		return builder.toString();
	}

	public boolean primaryKeyMatches(String primaryKey) {
	    return getPrimaryKey().equals(primaryKey);
	}

    public boolean primaryKeyMatches(String otherOwner, String otherChannel, String otherUri) {
		return (getUri().equals(otherUri) && getChannel().equals(otherChannel) && getOwner().equals(otherOwner));
	}

	public boolean primaryKeyMatches(Resource otherResource) {
		return primaryKeyMatches(otherResource.getOwner(), otherResource.getChannel(), otherResource.getUri());
	}

	// VALIDATORS

	public boolean hasValidUri() {
	    try {
	        URI testUri = URICreator.createUri(getUri());
	        return true;
		} catch (URISyntaxException e) {
	    	return false;
		}
	}

	// TODO: Should this require hasValidOwner ?
	public boolean isValid() {
	    return hasValidUri() && !containsTypeErrors;
	}

	public boolean hasValidOwner() {
		return !(getOwner().equals("*"));
	}

	public boolean hasValidNonFileUri() {
		try {
			URI testUri = URICreator.createUri(getUri());
			testUri.getScheme();
			if (testUri.getScheme() == null) {
				return false;
			}
			if (!testUri.getScheme().equals("file")) {
				return true;
			}
		} catch (URISyntaxException e) {}
		return false;
	}

	public boolean pointsToValidFile() {
		try {
			URI fileUri = URICreator.createUri(getUri());
			File f = new File(fileUri);
			if (f.exists() && f.isFile()) {
				return true;
			}
		} catch (URISyntaxException e) {
		} catch (IllegalArgumentException e) {
		}
		return false;
	}

	public boolean isValidPublish() {
		return isValid() && hasValidOwner() && hasValidNonFileUri();
	}

	public boolean isValidRemove() {
		return isValid() && hasValidOwner();
	}

	public boolean isValidShare() {
	    return isValid() && hasValidOwner() && pointsToValidFile();
	}

	// isValidQuery == isValidTemplate
	public boolean isValidTemplate() {
		// A ResourceTemplate with the owner "*" is still considered valid
		return !containsTypeErrors;
	}
	
	public boolean isValidType() {
		// A ResourceTemplate with the owner "*" is still considered valid
		return !containsTypeErrors;
	}

	public boolean isValidFetch() {
	    return isValidShare();
	}

	// Receives a resource template and returns true if the current resource's characteristics match

	public boolean getRelayValue(){ return isRelay;}
	public void setRelayValue(boolean value){ this.isRelay = value;}


	public boolean matchesResourceTemplate(Resource template) {
		boolean channelMatches = getChannel().equals(template.getChannel());

		boolean ownerMatches;
		String templateOwner = template.getOwner();
		if (templateOwner.isEmpty()) {
			ownerMatches = true;
		} else {
			ownerMatches = getOwner().equals(templateOwner);
		}

		boolean tagsMatch = true;
		for (String templateTag : template.getTags()) {
			boolean thisContainsTemplateTag = false;
			for (String thisTag : getTags()) {
				if (thisTag.equals(templateTag)) {
					thisContainsTemplateTag = true;
					break;
				}
			}
			if (!thisContainsTemplateTag) {
				tagsMatch = false;
			}
		}

		// I THINK this is the appropriate behaviour if a URI is not supplied, based on Aaron's demo commands
		boolean urisMatch = template.getUri().isEmpty() || getUri().equals(template.getUri());

		boolean nameMatches = false;
		boolean descriptionMatches = false;
		String templateName = template.getName();
		String templateDescription = template.getDescription();
		boolean templateNameAndDescriptionEmpty = templateName.isEmpty() && templateDescription.isEmpty();
		// Only bother to check the name and description match if it's not the case that
		// they're both empty, which will make the OR part of the return expression true
		// regardless
		if (!templateNameAndDescriptionEmpty) {
			nameMatches = getName().contains(templateName);
			descriptionMatches = getDescription().contains(templateDescription);
		}

		boolean relayCondition;
		if(template.getRelayValue()==true) {
			//Doesn't matter whether this is from a server or from a client
			relayCondition = true;
		}
		else{
			relayCondition	= (this.isRelay == template.getRelayValue()); //TODO potential bug
		}

		boolean sshMatches =(this.getSshValue()) == template.getSshValue();

		return channelMatches
				&& ownerMatches
				&& tagsMatch
				&& urisMatch
				&& relayCondition
				&& (nameMatches || descriptionMatches || templateNameAndDescriptionEmpty);
	}

	// Find the file under the URI, and return the size if it's a file, else -1
    public int getFileSizeAtUri() {
		int size = -1;
		try {
			URI fileUri = URICreator.createUri(getUri());
			File f = new File(fileUri);
			if (f.isFile()) {
				size = (int) f.length();
			} else {
				Logger.log(LogType.ERROR, "Resource's file does not exist: " + getUri());
				size = -1;
			}
		} catch (URISyntaxException e) {
			size = -1;
		} catch (IllegalArgumentException e) {
			size = -1;
		}
		// TODO: Remove this so Resources are truly immutable
		// TODO: This is potentially a race condition if it happens after a Resource is stored in a ResourceContainer
        return size;
    }

    public Resource cloneWithFileSizeFromUri() {
	    int newResourceSize = getFileSizeAtUri();
		return new Resource(getName(), getDescription(), getTags(), getUri(), getChannel(), getOwner(), getEzServer(), newResourceSize);
	}

    public Resource cloneWithAnonymousOwner() {
		String newOwner = getOwner().isEmpty() ? "" : "*";
	    return new Resource(getName(), getDescription(), getTags(), getUri(), getChannel(), newOwner, getEzServer(), getResourceSize());
	}

	public Resource clonewWithSubscriptionId(String newSubscriptionId) {
		 return new Resource(getName(), getDescription(), getTags(), getUri(), getChannel(), getOwner(), getEzServer(), getResourceSize(), newSubscriptionId);
	}

	// I consider 'filename' to be a single word
	public String getReceivedFilename() {
		// Returns a file directly friendly name that will be supplied to EZShare.Client when downloading resource
		String output = this.getEzServer() + this.getChannel() + this.getName();
		output = output.replaceAll("\\W+",""); //strips anything that isn't a number/letter/underscore; it replaces dots
		int lastIndexOfDot = getUri().lastIndexOf('.');
		// TODO: Subtract 1 because file could be a hidden file (e.g. .gitignore) with no extension?
		if (lastIndexOfDot > -1) {
			String fileExtension = getUri().substring(lastIndexOfDot);
			// Could a hidden folder in the path which is where the last . is
			if (!fileExtension.contains("/")) {
				output += fileExtension;
			}
		}
		if (output != null && !output.isEmpty()) {
			return output;
		} else {
			// A backup solution for situation that all the characters in attempted name are invalid
			return "downloadedFile";
		}
	}

	// Create a JSON representation of the Resource
	public JSONObject toJson() {
		JSONObject output = new JSONObject();
		output.put("name", name);
		output.put("description", description);
		output.put("uri", uri);
		output.put("channel", channel);
		output.put("owner", owner);
		output.put("ezserver", ezServer);
		JSONArray jsonTags = new JSONArray();
		for (String tag : getTags()) {
			jsonTags.add(tag);
		}
		output.put("tags", jsonTags);
		if (this.getResourceSize() > 0) {
			output.put("resourceSize", this.getResourceSize());
		}
		return output;
	}

	//
	private void setSubscriptionId(String newSubscriptionId) {
		subscriptionId = newSubscriptionId;
	}

	public String getSubscriptionId() {
		return subscriptionId;
	}
}
