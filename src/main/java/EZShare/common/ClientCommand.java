package EZShare.common;
import EZShare.logger.LogType;
import EZShare.logger.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.DefaultParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


import java.util.List;

//Overall class for dealing with the type of command when running from EZShare.client, gathers relevent information via string arguments

public class ClientCommand extends Command {
    private String host = "localhost";
    private int port = 3000;
    private boolean isSSL = false; //TODO update so this is variable

    public ClientCommand(String[] args) throws ParseException {
        Options options = getOptions();

        CommandLineParser parser = new DefaultParser();
        CommandLine commandLine = parser.parse(options, args);

        // Parse all argument values
        if (commandLine.hasOption("servers")) {
            String[] serverStringList = commandLine.getOptionValue("servers").split(",");
        	setServers(serverStringList);
        }
        if (commandLine.hasOption("secret")) {
        	setSecret(commandLine.getOptionValue("secret"));
        }
        isSSL = commandLine.hasOption("secure");
        // Ensure that we use the correct default port when using a secure
        // connection; this will be overridden by a supplied port
        if (isSSL) {
            setPort(3781);
        }
        if (commandLine.hasOption("port")) {
        	setPort(Integer.parseInt(commandLine.getOptionValue("port")));
        }
        if (commandLine.hasOption("host")) {
        	setHost(commandLine.getOptionValue("host"));
        }
        setDebug(commandLine.hasOption("debug"));

        // Resource stuff
        String id = "";
        String channel = "";
        String uri = "";
        String owner = "";
        String tags = "";
        String name = "";
        String description = "";
        String ezServer = "";
        
        if(commandLine.hasOption("id")) {
        	setId(commandLine.getOptionValue("id")) ;
        }
        
        if(commandLine.hasOption("channel")) {
            channel = commandLine.getOptionValue("channel");
        }
        if(commandLine.hasOption("owner")) {
            owner = commandLine.getOptionValue("owner");
        }
        if (commandLine.hasOption("tags")) {
            tags = commandLine.getOptionValue("tags");
        }
        if(commandLine.hasOption("name")) {
            name = commandLine.getOptionValue("name");
        }
        if(commandLine.hasOption("description")) {
            description = commandLine.getOptionValue("description");
        }
        if(commandLine.hasOption("uri")){
            uri = commandLine.getOptionValue("uri");
        }

        setResource(new Resource(name, description, tags, uri, channel, owner, ""));
        //setSubscription(new Resource(name, description, tags, uri, channel, owner, ""), getId(), relay);

        if (commandLine.hasOption("id")) {
            setId(commandLine.getOptionValue("id"));
        }

        if (commandLine.hasOption("exchange")) {
            commandType = CommandType.EXCHANGE;
        } else if (commandLine.hasOption("fetch")) {
            commandType = CommandType.FETCH;
        } else if (commandLine.hasOption("publish")) {
            commandType = CommandType.PUBLISH;
        } else if (commandLine.hasOption("query")) {
            commandType = CommandType.QUERY;
        }else if (commandLine.hasOption("subscribe")) {
            commandType = CommandType.SUBSCRIBE;
        }  else if (commandLine.hasOption("unsubscribe")) {
            commandType = CommandType.UNSUBSCRIBE;
        }  
        else if (commandLine.hasOption("remove")) {
            commandType = CommandType.REMOVE;
        } else if (commandLine.hasOption("share")) {
            commandType = CommandType.SHARE;
            if (!hasSecret()) {
                throw new ParseException("Need a secret");
            }
        } else {
            commandType = CommandType.INVALID;
        }

    }
    
    public ClientCommand(CommandType newCommandType, ServerAddress server, Resource newResource, List<ServerAddress> serverList, boolean relayValue, boolean SSLvalue) {
    	// for EZShare.client.server to send either a query or a exchange command
    	commandType = newCommandType;
    	if (newResource != null) {
    		setResource(newResource);
    	}
    	else {
            // Set a blank resource to go with command
            setResource(new Resource("", "", "", "", "", "", ""));
        }

		setHost(server.getHostname());
		setPort(server.getPort());
    	if (serverList != null) {
    		for(ServerAddress newServer : serverList) {
    			addServer(newServer);
    		}
    	}
        setRelay(relayValue); // we dont relay if is comming from EZShare.client.server
    }

    private ClientCommand(CommandType newCommandType, String newId) {
        commandType = newCommandType;
        setId(newId);
    }

    public static ClientCommand unsubscribeCommand(String id) {
        return new ClientCommand(CommandType.UNSUBSCRIBE, id);
    }

    public static Options getOptions() {
        // Initialize commands and return back all relevant ones
        Options options = new Options();

        // initialize options with all possible commands
        options.addOption("id",true,"input id");
        options.addOption("channel",true,"input channel");
        options.addOption("description",true, "input description");
        options.addOption("host",true,"host");
        options.addOption("name",true,"name");
        options.addOption("owner",true,"owner");
        options.addOption("port",true,"port");
        options.addOption("secret",true,"secret");
        options.addOption("servers",true,"servers");
        options.addOption("tags",true,"tags");
        options.addOption("uri",true,"uri");
        options.addOption("debug", false, "activate debugger");
        options.addOption("exchange", false, "exchange");
        options.addOption("fetch",false,"fetch");
        options.addOption("publish",false,"publish");
        options.addOption("query",false,"query");
        options.addOption("remove",false,"remove");
        options.addOption("share",false,"share");
        options.addOption("subscribe",false,"subscribe");
        options.addOption("unsubscribe",false,"unsubscribe");
        options.addOption("secure", false, "use SSL connection");
        return options;
    }
    
    public JSONObject toJson() {
        // reads the command from arguments, creates appropriate json command in response
        JSONObject output = new JSONObject();
        //TODO add ezserver
        JSONObject jsonResource = getResource().toJson();

        switch(getCommandType()) {
            case PUBLISH:
            	output.put("resource", jsonResource);
            	output.put("command", "PUBLISH");
                
                break;
            case REMOVE:
                output.put("command", "REMOVE");
                output.put("resource", jsonResource);
                break;
            case SHARE:
                output.put("command", "SHARE");
                output.put("secret", this.getSecret());
                output.put("resource", jsonResource);
                break;
            case QUERY:
                output.put("command", "QUERY");
                output.put("relay", Boolean.valueOf(getRelay()));
                output.put("resourceTemplate", jsonResource);
                break;
            case SUBSCRIBE:
                output.put("command", "SUBSCRIBE");
                output.put("relay", Boolean.valueOf(getRelay()));
                output.put("resourceTemplate", jsonResource);
                output.put("id", getId());
                break;
            case UNSUBSCRIBE:
            	output.put("command", "UNSUBSCRIBE");
            	output.put("id", getId());
            	break;
            case FETCH:
                output.put("command", "FETCH");
                output.put("resourceTemplate", jsonResource);
                break;
            case EXCHANGE:
                output.put("command", "EXCHANGE");
                JSONArray serverArray = new JSONArray();
                for (ServerAddress serverAddress : getServers()) {
                    serverArray.add(serverAddress.toJson());
                }
                output.put("serverList", serverArray);
                break;
            case INVALID:
                output.put("command", "INVALID");
                break;
        }
        return output;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String newHost) {
        host = newHost;
    }

    public boolean isSSL(){
        return isSSL;
    }
    
    public int getPort() {
        return port;
    }

    public void setPort(int newPort) {
        port = newPort;
    }
}
