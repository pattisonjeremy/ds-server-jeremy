/**
 * <h1>Client</h1>
 * <p>The Client class is the top class for the EZShare.client side.</p>
 * <p>In the EZShare.client class, we use the ClientCommand class to read/save all the command user gave to EZShare.client,</p>
 * <p>and use the ClientConnection class to connect with the EZShare.client.server and send the commands to the EZShare.client.server.</p>
 * 
 * 	 
 * @author Group 88888
 * @version 1.0
 * 
 */
package EZShare;

import EZShare.common.ClientSubscribe;
import org.apache.commons.cli.HelpFormatter;

import EZShare.logger.LogType;
import EZShare.logger.Logger;
import EZShare.common.ClientCommand;
import EZShare.common.ClientConnection;

import java.io.IOException;
import java.util.Scanner;

public class Client {
	public static void main(String[] args) {
		// We use ClientCommand class to read/save all the commands
		ClientCommand command;
		try {
			command = new ClientCommand(args);
			Logger.initialize("Client", command.isDebug());
			Logger.log(LogType.INFO, "Parsed arguments");
		} catch (org.apache.commons.cli.ParseException e) {
			Logger.initialize("Client", false);
			Logger.log(LogType.INFO, "ERROR: Cannot parse arguments");
			Logger.flush();
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("EZServer Client", ClientCommand.getOptions());
			return;
		}

		// Following 3 lines from tute
		// Location of the Java keystore file containing the collection of
		// certificates trusted by this application (trust store).
		//System.setProperty("javax.net.ssl.trustStore", "serverKeys/server88888.jks");
		//System.setProperty("javax.net.ssl.trustStore", "clientKeys/client.crt");
		//System.setProperty("javax.net.ssl.trustStore", "clientKeys/rootCA.pem");



		//Password to access the private key from the keystore file
		System.setProperty("javax.net.ssl.keyStore", "clientKeys/client88888.jks");
		System.setProperty("javax.net.ssl.keyStorePassword", "team88888");

		System.setProperty("javax.net.ssl.trustStore", "clientKeys/client88888.jks");

		System.setProperty("javax.net.sll.trustStorePassword", "team88888");





		//System.setProperty("javax.net.debug", "all");

        // Connect and handle response
		ClientConnection connection = new ClientConnection(command, false);
		ClientSubscribe subscription = connection.getSubscription();
		if (subscription != null) {
			Scanner scanner = new Scanner(System.in);
			while (true) {
				Logger.log(LogType.DEBUG,"Awaiting client to terminate");
				if (scanner.hasNext()) {
					Logger.log(LogType.INFO, "Sending unsubscribe command");
					try {
						subscription.sendUnsubscribe(command.getId());
					} catch (IOException e) {
						Logger.log(LogType.ERROR, "Subscription connection not properly closed");
						Logger.flush();
						return;
					}
					break;
				} else {
					Logger.log(LogType.ERROR,"System did not terminate subscription properly");
				}
			}
		}


		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			Logger.log(LogType.ERROR,"System interrupt detected");
		}


		Logger.log(LogType.INFO, "Shutting down");
		Logger.flush();
	}

}
