package EZShare.logger;

import java.util.concurrent.ConcurrentLinkedQueue;

// Logging daemon that wakes up several times a second to print
// all the log messages in its queue
public class LoggerRunnable implements Runnable {


    public LoggerRunnable() {}

    public void run() {
        while (true) {
            try {
                Logger.flush();
                Thread.sleep(200);
            } catch (InterruptedException e) {}
        }
    }

}
