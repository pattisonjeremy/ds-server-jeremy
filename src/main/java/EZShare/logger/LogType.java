/**
 * <h2>LogType</h2>
 * <p>The LogType separate the message into 3 different kinds.</p>
 * <p>The first is the INFO type, which is the system message during the connection. It will always be printed out.</p>
 * <p>The second is SEND type, which is the message Client send to the Server. When the config debugger set on it will be printed.</p>
 * <p>The third is RECIEVE type, which is the message Server send to the Client. When the config debugger set on it will be printed.</p>
 * 
 * @author Group 88888
 * @version 1.0
 * 
 */
package EZShare.logger;

public enum LogType {
	INFO, DEBUG, SENT, RECEIVED, ERROR
	// INFO/ERROR is things always to print
	// DEBUG/SENT/RECEIVED are only printed if debugger set on
}
