/**
 * <h1>Logger</h1>
 * <p>TheLogger class let the terminal print out the messages during the connecting between EZShare.client.server and EZShare.client.</p>
 *
 * <p>It separate the message into 3 types, and add the system time before print these messages</p>
 * 
 * @author Group 88888
 * @version 1.0
 * 
 */
package EZShare.logger;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.concurrent.ConcurrentLinkedQueue;

// Thread-safe logger
public class Logger {

	private static String name = "System";
	private static boolean isDebug = false;
	// ConcurrentLinkedQueues are thread-safe without needing to wait for locks! I love them now
	private static ConcurrentLinkedQueue<String> queue = new ConcurrentLinkedQueue<>();
	private static Thread loggerDaemon = null;
	
	public static void initialize(String newName, boolean debugValue) {
		name = newName;
		isDebug = debugValue;
		if (loggerDaemon != null && loggerDaemon.isAlive()) {
			System.out.println("ERROR: Logger already exists!");
			return;
		}
		loggerDaemon = new Thread(new LoggerRunnable());
		loggerDaemon.setDaemon(true);
		loggerDaemon.start();
		log(LogType.INFO, "Logger initialized");
	}

	// Pushes messages onto a queue so that no locking is required
	public static void log(LogType type, String message) {
		StringBuilder output = new StringBuilder();
		String timeStamp = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss").format(new Date());
		output.append(timeStamp);
		output.append("-["); output.append(name); output.append("]-");
		if (type.equals(LogType.INFO) || type.equals(LogType.ERROR)) {
			output.append("["); output.append(type.toString()); output.append("]-"); output.append(message);
			queue.add(output.toString());
			return;
		}
		if (isDebug && (type.equals(LogType.RECEIVED) || type.equals(LogType.SENT) || type.equals(LogType.DEBUG))) {
			output.append("[DEBUG]-"); output.append(type.toString()); output.append(": "); output.append(message);
			queue.add(output.toString());
			return;
		}
	}

	public static void flush() {
	    String output;
		while ((output = queue.poll()) != null) {
			System.out.println(output);
		}
	}

	public static boolean isAlive() {
		return loggerDaemon.isAlive();
	}
}
